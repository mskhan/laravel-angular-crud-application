<p ng-if="authenticatedUser">
    Hello {{authenticatedUser.username}}, Authenticated User
</p>
<p ng-if="!authenticatedUser">
    <h1>Welcome to my first demo application</h1>
</p>
