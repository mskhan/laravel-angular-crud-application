<div ng-controller="EmployeeController">
    <form class="form-horizontal" ng-submit="create()" novalidate>
        <fieldset>
            <div class="form-group">
                <div class="col-md-5">
                    <input id="body" name="name" type="text" placeholder="Enter Employee Name" class="form-control input-md"
                           ng-model="name" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-5">
                    <input id="email" name="descriptions" placeholder="Enter Employee Email" class="form-control input-md"
                              ng-model="email" required>

                </div>
            </div>
            <div class="form-group">
                <div class="col-md-5">
                    <input type="submit" id="submit" name="submit" class="btn btn-primary"/>
                </div>
            </div>
        </fieldset>
    </form>
</div>


