<div ng-controller="EmployeeController" ng-init="find()">
    <p ng-if="!employees.length">
        There are no employee registered now, <a href="/employees/create">create one!</a>
    </p>

    <div class="row">
        <div class="col-lg-6" ng-repeat="employee in employees">
            <div class="input-group employee-list">
                <span class="input-group-addon">
                    <input type="button" ng-click="remove(employee)" name="Remove" value="Remove">
                    <button ng-click="detail(employee.id)">Detail</button>

                </span>
             
                <input type="text" class="form-control" ng-model="employee.name"
                       ng-blur="update(employee)" enter-stroke="update(employee)">
                <input type="text" class="form-control" ng-model="employee.email"
                       ng-blur="update(employee)" enter-stroke="update(employee)">
            </div>

        </div>
    </div>
</div>
<style>
    .employee-list {
        margin: 5px 0px;
    }


</style>

