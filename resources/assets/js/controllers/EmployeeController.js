angular.module('EmployeeController', []).controller('EmployeeController', ['$scope', '$location','$routeParams','Employee',
    function ($scope, $location, $routeParams, Employee) {
    $scope.create = function () {
        var employee = new Employee({
            name: this.name,
            email: this.email
        });
        employee.$save(function (res){
            $location.path('employees/view/' + res.id);
            $scope.name = '';
            $scope.email = ''
        });
    }
        $scope.detail = function (id) {
            $location.path('employees/view/' + id);
        }
        $scope.findOne = function () {
            var splitPath = $location.path().split('/');
            var employeeId = splitPath[splitPath.length - 1];
            $scope.employee = Employee.get({employeeId: employeeId});
        };

        $scope.find = function () {
            $scope.employees = Employee.query();
        };


        $scope.remove = function (employee) {
            employee.$remove(function (res) {
                if (res) {
                    for (var i in $scope.employees) {
                        if ($scope.employees[i] === employee) {
                            $scope.employees.splice(i, 1);
                        }
                    }
                }
            }, function (err) {
                console.log(err);
            })
        };

        $scope.update = function (employee) {
            employee.$update(function (res) {
            }, function (err) {
                console.log(err);
            });
        };
}])