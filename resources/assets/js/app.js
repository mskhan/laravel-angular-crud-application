angular.module('todoApp', [
  'ngRoute',
  'ngResource',
  'ngStorage',
  'appRoutes',
  'enterStroke',
  'MainController',
  'TodoController',
  'UserController',
  'EmployeeController',
  'UserService',
  'EmployeeService',
  'TodoService',
]);

