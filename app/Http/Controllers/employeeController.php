<?php

namespace App\Http\Controllers;

use App\employeeModel;
use Illuminate\Http\Request;

class employeeController extends Controller

{
    public function index()
    {
        return employeeModel::all();
    }

    public function store(Request $request)
    {
        $inputs = $request->all();
        $data = new employeeModel($inputs);
        if (!$data->save()) {
            abort(500, "Saving failed.");
        }
        return $data;
    }

    public function show($id)
    {
        return employeeModel::find($id);
    }

    public function update($id, Request $request)
    {
        $employee = employeeModel::find($id);
        $employee->name = $request->input('name');
        $employee->email = $request->input('email');
        if (!$employee->save()) {
            abort(500, "Saving failed");
        }
        return $employee;
    }

    public function destroy($id)
    {
        return employeeModel::destroy($id);
    }
}