<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employeeModel extends Model
{
    protected $table = 'employees';

    protected $fillable = [
        'name', 'email'
    ];
}
